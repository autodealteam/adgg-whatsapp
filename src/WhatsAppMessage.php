<?php

namespace Autodeal\Whatsapp;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Exception\GuzzleException;

class WhatsAppMessage
{

    private $to;
    private $parameters;
    private $template;
    private $dynamicUrlButtons;

    const AUTODEAL_WHATSAPP_ACCESS_TOKEN = 'autodeal-whatsapp-access-token';


    public function send()
    {
        try {
            $json = $this->json();
            $client = new Client([
                'base_uri' => 'https://graph.facebook.com/v19.0/'
            ]);

            $phoneNumberId = config('whatsapp.phone_number_id');
            // $token = config('whatsapp.access_token');
            $token =  DB::table('configurations')->where('identifier', self::AUTODEAL_WHATSAPP_ACCESS_TOKEN)->first();

            $client->request('POST', "$phoneNumberId/messages", [
                'json' => $json,
                'headers' => [
                    'Authorization' => "Bearer {$token->content}",
                    'Content-Type' => 'application/json'
                ]
            ]);
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }

    private function json()
    {
        $json = [
            'messaging_product' => 'whatsapp',
            'to' => $this->to,
            'type' => 'template',
        ];

        if ($this->template) $json['template'] =  $this->template;

        if ($this->template && ($this->parameters && count($this->parameters) > 0))
            $json['template']['components'][] = [
                'type' => 'body',
                'parameters' => $this->parameters
            ];

        if ($this->template && $this->dynamicUrlButtons) {
            foreach ($this->dynamicUrlButtons as $dynamicUrlButton) {
                $json['template']['components'][] = $dynamicUrlButton;
            }
        }

        return $json;
    }

    public function templateName($name)
    {
        $this->template = ['name' => $name, 'language' => ['code' => 'es_MX']];
        return $this;
    }

    public function parameters(array $params)
    {
        $this->parameters = array_map(fn($p) => ['type' => 'text', 'text' => ($p . '')], $params);
        return $this;
    }

    public function to($to)
    {
        $to =  preg_replace('/^52|[^0-9]+/', '', $to);
        $this->to = '52' . $to;
        return $this;
    }

    public function buttonUrl($index, $uri)
    {
        $this->dynamicUrlButtons = $this->dynamicUrlButtons ?? [];
        $this->dynamicUrlButtons[] = [
            'type' => 'button',
            'sub_type' => 'url',
            'index' => $index,
            'parameters' => [
                [
                    'type' => 'text',
                    'text' => $uri
                ]
            ]
        ];
        return $this;
    }
}
