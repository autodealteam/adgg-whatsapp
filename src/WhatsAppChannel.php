<?php

namespace Autodeal\Whatsapp;

use Illuminate\Notifications\Notification;

class WhatsAppChannel 
{
    public function send($notifiable, Notification $notification)
    {
        $notification->toWhatsapp($notifiable)->send();
    }
}