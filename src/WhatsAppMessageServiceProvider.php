<?php

namespace Autodeal\Whatsapp;

use Illuminate\Support\ServiceProvider;

class WhatsAppMessageServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/whatsapp.php' => config_path('whatsapp.php')
        ]);
    }

/*     public function register()
    {
        $this->app->singleton(WhatsappMessage::class, function() {
            return new WhatsappMessage();
        });
    } */
}