<?php

return [
    'phone_number_id' => env('AUTODEAL_WHATSAPP_PHONE_NUMBER_ID', null),
    'access_token' => env('AUTODEAL_WHATSAPP_ACCESS_TOKEN', null)
];